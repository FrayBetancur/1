$(function () {
       $('[data-toggle="tooltip"]').tooltip();
       $('[data-toggle="popover"]').popover();
       $('.carousel').carousel({
          interval: 3000
       })
       $('#exampleModal').on('show.bs.modal', function(e){
        console.log('el modal se está mostrando');
        $('#registro').removeClass('btn-outline-dark');
        $('#registro').addClass('btn-danger');
        $('#registro').prop('disabled', true)
    });
    $('#exampleModal').on('shown.bs.modal', function(e){
        console.log('el modal se mostró');
    });
    $('#exampleModal').on('hide.bs.modal', function(e){
        console.log('el modal se oculta');
    });
    $('#exampleModal').on('hidden.bs.modal', function(e){
        console.log('el modal se ocultó');
        $('#registro').prop('disabled', false);
        $('#registro').addClass('btn-outline-dark')
    })
});
